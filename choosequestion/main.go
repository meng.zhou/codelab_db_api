package main

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"../../../../Desktop/codelab_DB"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration

// Handler is our lambda handler invoked by the `lambda.Start` function call
type ChooseRequest struct{
	InterviewID int64
	QuestionID int64
}


func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var choose_request ChooseRequest
	byt := []byte(request.Body)
	err := json.Unmarshal(byt, &choose_request)

	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 400,}, err
	}

	err = codelab_DB.AddQuestionToInterview(choose_request.InterviewID, choose_request.QuestionID)

	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 400,}, err
	}

	resp := events.APIGatewayProxyResponse{StatusCode:200,}

	return resp, nil
}

func main() {
	lambda.Start(Handler)
}