package main

import (
	"bytes"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"../../../../Desktop/codelab_DB"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	interviewers, db_err := codelab_DB.GetAllInterviewer()
	if db_err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 400,}, db_err
	}

	var buf bytes.Buffer

	body, err := json.Marshal(interviewers)
	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 400,}, err
	}
	json.HTMLEscape(&buf, body)

	resp := events.APIGatewayProxyResponse{
		StatusCode:      200,
		IsBase64Encoded: false,
		Body:            buf.String(),
	}

	return resp, nil
}

func main() {
	lambda.Start(Handler)
}
