package main

import (
	"bytes"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"../../../../Desktop/codelab_DB"
)

type InterviewInfo struct {
	Interview codelab_DB.Interview
	CandidateInfo codelab_DB.Candidate
	InterviewerInfo codelab_DB.Interviewer
}

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	interviews, db_err := codelab_DB.GetAllInterview()
	if db_err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 400,}, db_err
	}
	candidates, db_err2 := codelab_DB.GetAllCandidate()
	if db_err2 != nil {
		return events.APIGatewayProxyResponse{StatusCode: 400,}, db_err
	}
	interviewers, db_err3 := codelab_DB.GetAllInterviewer()
	if db_err3 != nil {
		return events.APIGatewayProxyResponse{StatusCode: 400,}, db_err
	}

	interviewInfo := make([]InterviewInfo, len(interviews))

	for i := 0; i < len(interviews); i++ {
		candidate_id := interviews[i].CandidateId
		interviewer_id := interviews[i].InterviewerId
		var candidate codelab_DB.Candidate
		var interviewer codelab_DB.Interviewer
		for j := 0; j < len(candidates); j++ {
			if candidates[i].ID == candidate_id {
				candidate = candidates[i]
				break
			}
		}
		for j := 0; j < len(interviewers); j++ {
			if interviewers[i].ID == interviewer_id {
				interviewer = interviewers[i]
				break
			}
		}
		interviewInfo[i] = InterviewInfo{Interview: interviews[i], CandidateInfo: candidate, InterviewerInfo: interviewer}
	}

	var buf bytes.Buffer

	body, err := json.Marshal(interviewInfo)
	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 400,}, err
	}
	json.HTMLEscape(&buf, body)

	resp := events.APIGatewayProxyResponse{
		StatusCode:      200,
		IsBase64Encoded: false,
		Body:            buf.String(),
	}

	return resp, nil
}

func main() {
	lambda.Start(Handler)
}
