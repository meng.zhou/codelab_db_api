.PHONY: build clean deploy

build:
	dep ensure -v
	env GOOS=linux go build -ldflags="-s -w" -o bin/hello hello/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/world world/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/question question/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/addquestion addquestion/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/choosequestion choosequestion/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/unchoosequestion unchoosequestion/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/addinterview addinterview/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/interview interview/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/addinterviewer addinterviewer/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/addcandidate addcandidate/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/candidate candidate/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/interviewer interviewer/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/getchosenquestion getchosenquestion/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/getunchosenquestion getunchosenquestion/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/addsolution addsolution/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/getsolution getsolution/main.go

clean:
	rm -rf ./bin ./vendor Gopkg.lock

deploy: clean build
	sls deploy --verbose
